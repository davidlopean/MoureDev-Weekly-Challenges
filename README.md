<h1>MoureDev-Weekly-Challenges</h1>

> [!NOTE]
> En este repositorio voy a subir la solución a los retos de programación semanales propuestos por Moure Dev.

![image](https://github.com/davidlopean/MoureDev-Weekly-Challenges/assets/141661643/27f53e29-ed2e-4141-a395-47f50b5b5f67)

## Listado de retos 2023
0. ✅ EL FAMOSO "FIZZ BUZZ" | Soluciones: [[PYTHON](http://google.es)]
1. ⬜ EL "LENGUAJE HACKER" | Soluciones:
2. ⬜ EL PARTIDO DE TENIS | Soluciones:
3. ⬜ EL GENERADOR DE CONTRASEÑAS | Soluciones:
4. ⬜ PRIMO, FIBONACCI Y PAR | Soluciones: 
5. ⬜ HOLA MUNDO | Soluciones: 
6. ⬜ PIEDRA, PAPEL, TIJERA, LAGARTO, SPOCK | Soluciones: 
7. ⬜ EL SOMBRERO SELECCIONADOR | Soluciones:
8. ⬜ EL GENERADOR PSEUDOALEATORIO | Soluciones: 
9. ⬜ HETEROGRAMA, ISOGRAMA Y PANGRAMA | Soluciones: 
10. ⬜ LA API | Soluciones: 
11. ⬜ URL PARAMS | Soluciones: 
12. ⬜ VIERNES 13 | Soluciones:
13. ⬜ ADIVINA LA PALABRA | Soluciones:
14. ⬜ OCTAL Y HEXADECIMAL | Soluciones:
16. ⬜ AUREBESH | Soluciones:
17. ⬜ LA ESCALERA | Soluciones:
18. ⬜ GIT Y GITHUB | Soluciones:
19. ⬜ WEB SCRAPING | Soluciones:
20. ⬜ ANÁLISIS DE TEXTO | Soluciones: 
21. ⬜ LA TRIFUERZA | Soluciones:
22. ⬜ NÚMEROS PRIMOS GEMELOS | Soluciones:
23. ⬜ LA ESPIRAL | Soluciones:
24. ⬜ LA BASE DE DATOS | Soluciones:
25. ⬜ CIFRADO CÉSAR | Soluciones:
26. ⬜ EL CÓDIGO KONAMI | Soluciones:
27. ⬜ TESTING | Soluciones:
28. ⬜ CUENTA ATRÁS | Soluciones:
29. ⬜ EXPRESIÓN MATEMÁTICA | Soluciones:
30. ⬜ EL CARÁCTER INFILTRADO | Soluciones:
31. ⬜ EL TECLADO T9 | Soluciones:
32. ⬜ EL ÁBACO | Soluciones:
33. ⬜ LA COLUMNA DE EXCEL | Soluciones:
34. ⬜ TETRIS | Soluciones:
35. ⬜ EL TXT | Soluciones:
36. ⬜ PRIMEROS PASOS | Soluciones:
37. ⬜ PERMUTACIONES | Soluciones:
38. ⬜ COLORES HEX Y RGB | Soluciones:
39. ⬜ LAS SUMAS | Soluciones:
40. ⬜ TRIPLES PITAGÓRICOS | Soluciones:
41. ⬜ TABLA DE MULTIPLICAR | Soluciones:
42. ⬜ LA CASA ENCANTADA | Soluciones:
43. ⬜ PUNTO DE ENCUENTRO | Soluciones:
44. ⬜ SIMULADOR DE CLIMA | Soluciones:
45. ⬜ ADIVINANZAS MATEMÁTICAS | Soluciones:
46. ⬜ EL CALENDARIO DE ADEVIENTO 2023 | Soluciones:
47. ⬜ LA CARRERA DE COCHES | Soluciones:
48. ⬜ LA PALABRA DE 100 PUNTOS | Soluciones:
49. ⬜ EL RANKING | Soluciones:


![image](https://github.com/davidlopean/MoureDev-Weekly-Challenges/assets/141661643/e81e6303-de1d-459b-a824-4d7da7acd0ab)

## Listado de retos 2024

0. ⬜ SINTAXIS, VARIABLES, TIPOS DE DATOS Y HOLA MUNDO | Soluciones:
1. ⬜	OPERADORES Y ESTRUCTURAS DE CONTROL | Soluciones:
2. ⬜	FUNCIONES Y ALCANCE | Soluciones:
3. ⬜	ESTRUCTURAS DE DATOS | Soluciones:
4. ⬜	CADENAS DE CARACTERES | Soluciones:
5. ⬜	VALOR Y REFERENCIA | Soluciones:
6. ⬜	RECURSIVIDAD | Soluciones:
7. ⬜	PILAS Y COLAS | Soluciones:
8. ⬜	CLASES | Soluciones:
9. ⬜	HERENCIA Y POLIMORFISMO | Soluciones:
10. ⬜ EXCEPCIONES | Soluciones:
